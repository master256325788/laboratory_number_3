#include<stdio.h>

void displayArray(int arr[], int column, int str){
    int i, j;
    printf("The complete array is: \n");
    for (i = 0; i < column; i++){
        printf("\n");
        for (j = 0; j < str; j++){
            printf("%d\t", arr[i*str + j]);
        }
    }
}

int main(){
    int column;
    int str;

    int i, j;

    printf("enter the numbers of rows and columns:\n");
    scanf("%d %d", &str, &column);

    int arr[column*str];
    printf("Please enter %d numbers for the array: \n", str*column);
    for (i = 0; i < column; i++){
        for (j = 0; j < str; j++){
            scanf("%d", &arr[i*str + j]);
	}
    }

    int indexMinStr, indexMaxStr, indexMinCol, indexMaxCol;
    int minStr, maxCol;

    for (i = 0; i < column; i++){
	minStr = arr[i*str];
	indexMinStr = i;
	indexMinCol = 0;

        for (j = 0; j < str; j++){
	    if (minStr  > arr[indexMinStr*str + j]){
		minStr = arr[indexMinStr*str + j];
		indexMinCol = j;
	    }
	}
	maxCol = arr[indexMinCol];
	indexMaxStr = 0;
	indexMaxCol = indexMinCol;
	for (int f = 0; f < column; f++){
	    if (maxCol < arr[f*str + indexMinCol]){
		maxCol = arr[f*str + indexMinCol];
		indexMaxStr = f;
	    }
	}
	if (minStr == maxCol && indexMinStr == indexMaxStr && indexMinCol == indexMaxCol ){
	    printf("\nsaddle point:%d {%d %d}\n", minStr, indexMinStr, indexMinCol);
	    printf("minStr = %d {%d, %d}\n", minStr, indexMinStr, indexMinCol);
	    printf("maxCol = %d {%d, %d}\n", maxCol, indexMaxStr, indexMaxCol);
    }
    displayArray(arr, column , str);
    return 0;
    }
}


